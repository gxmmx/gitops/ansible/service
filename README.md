# Ansible Collection - gxmmx.service

Ansible collection for various services.  
Includes content to setup and configure frequently used programs.

## Information

[![Gitlab Repo][badge-repo-gitlab]][url-repo-gitlab]
[![Github Repo][badge-repo-github]][url-repo-github]
[![Ansible Galaxy][badge-repo-galaxy]][url-repo-galaxy]

[![Documentation][badge-documentation]][url-documentation]

### Release

[![Latest Release][badge-latest-release]][url-latest-release]
[![Latest Pipeline][badge-latest-pipeline]][url-latest-pipeline]
[![Latest Commit][badge-latest-commit]][url-latest-commit]

### Tools

[![GxMMx Flow][badge-flow-attribution]][url-flow-attribution]
[![Ansible][badge-ansible-attribution]][url-ansible-attribution]

## [Documentation][url-documentation]

### Installation

#### Ansible Galaxy installation

**`＞`**

``` shell
ansible-galaxy collection install gxmmx.service
```

#### Git installation

**`＞`**

``` shell
dir="~/.ansible/collections/ansible_collections/gxmmx"
mkdir -p "$dir"
git clone "https://gitlab.com/gxmmx/gitops/ansible/service.git" "${dir}/linux"
```

### Usage

Playbook example that uses the `gxmmx.service.xxx` to...
See [documentation][url-documentation] for more examples.

**📄 `site.yml`**

``` yaml
- hosts: all

```

## [Changes][url-changelog]

Version history with features and bugfixes,
as well as upcoming features and roadmap  
depicted in [`CHANGELOG.md`][url-changelog]

## [Contributing][url-contributing]

Any contributions are greatly appreciated.  
See [`CONTRIBUTING.md`][url-contributing] for more information.

### Contributors

* [gummigudm](https://gitlab.com/gummigudm)  

## [License][url-license]

Distributed under the MIT License.  
See [`LICENSE`][url-license] for more information.

## [Contact](https://gitlab.com/gummigudm)

Guðmundur Guðmundsson - [gummigudm@gmail.com](mailto:gummigudm@gmail.com)

* Gitlab - [gummigudm](https://gitlab.com/gummigudm)  
* Github - [gummigudm](https://github.com/gummigudm)  

[//]: # (Generic links)
[badge-repo-gitlab]: https://img.shields.io/badge/Gitlab-Repo-blue?logo=gitlab
[badge-repo-github]: https://img.shields.io/badge/Github-Repo-blue?logo=github
[badge-repo-galaxy]: https://img.shields.io/badge/Ansible_Galaxy-Repo-red?logo=ansible
[badge-documentation]: https://img.shields.io/badge/Docs-Gitlab_Wiki-blue?logo=googledocs

[url-flow-attribution]: https://gitlab.com/gxmmx/gitops/gitlab/flow
[badge-flow-attribution]: https://img.shields.io/badge/GxMMx-Flow-blue?logo=gitlab

[url-ansible-attribution]: https://ansible.com
[badge-ansible-attribution]: https://img.shields.io/badge/Ansible-Core-red?logo=ansible

[//]: # (Project specific links)
[url-repo-gitlab]: https://gitlab.com/gxmmx/gitops/ansible/service
[url-repo-github]: https://github.com/gxmmx/gitops-ansible-service
[url-repo-galaxy]: https://galaxy.ansible.com/gxmmx/service
[url-documentation]: https://gitlab.com/gxmmx/gitops/ansible/service/-/wikis/home
[url-license]: https://gitlab.com/gxmmx/gitops/ansible/service/-/blob/main/LICENSE
[url-changelog]: https://gitlab.com/gxmmx/gitops/ansible/service/-/blob/main/CHANGELOG.md
[url-contributing]: https://gitlab.com/gxmmx/gitops/ansible/service/-/blob/main/CONTRIBUTING.md

[url-latest-release]: https://gitlab.com/gxmmx/gitops/ansible/service/-/releases
[badge-latest-release]: https://img.shields.io/gitlab/v/release/gxmmx%2Fgitops%2Fansible%2Fservice

[url-latest-pipeline]: https://gitlab.com/gxmmx/gitops/ansible/service/-/pipelines
[badge-latest-pipeline]: https://img.shields.io/gitlab/pipeline-status/gxmmx%2Fgitops%2Fansible%2Fservice

[url-latest-commit]: https://gitlab.com/gxmmx/gitops/ansible/service/-/commits/main?ref_type=heads
[badge-latest-commit]: https://img.shields.io/gitlab/last-commit/gxmmx%2Fgitops%2Fansible%2Fservice
