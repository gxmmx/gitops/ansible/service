# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- markdownlint-disable MD012 MD024 -->

## Unreleased

- IP filters

## [0.1.0] - 2024-01-31

### Added

- Certificate Authority role
